import React from "react";
import { StyleSheet } from "react-native";

const Estilo = StyleSheet.create ({
    flexPai: {
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: "4%",
        backgroundColor: "CACACA"
    },
    menuPrincipal: {
        display: "flex",
        flex: 1
    },
    textoHome: {
        fontSize: 25,
        borderWidth: 1,
        borderColor: 'black',
        padding: 10,
        backgroundColor: "white",
        color: "black"
    },
    textoCadastrar: {
        fontSize: 22,
        borderWidth: 1,
        borderColor: 'black',
        padding: 2,
        margin: 8,
        backgroundColor: "white",
        textAlign:"center",
        color: "black"
    },
    componenteInputsCadastro: {
        display: "flex",
        flex: 1,
        alignItems:"center",
        paddingBottom:"12%"
    },
    textoInputsCadastro: {
        fontSize: 20,
        color:"black"
    },
    flexPaiNotas: {
        display: "flex",
        flex: 1,
        justifyContent: "center",
        padding: "4%",
        backgroundColor: "CACACA"
    },
    textoLabelNotas: {
        fontSize: 18,
        color: "black"
    },
    componenteSelectNotas: {
        paddingHorizontal:"12%",
        paddingBottom:"8%"
    },
    botoesNotas: {
        fontSize: 22,
        borderWidth: 1,
        borderColor: 'black',
        padding: 2,
        margin: 8,
        backgroundColor: "white",
        textAlign:"center",
        color: "black"
    },
    flexPaiPerfil: {
        display: "flex",
        flex: 1,
        justifyContent: "center",
        padding: "4%",
        backgroundColor: "CACACA"
    },
    textoLabelPerfil: {
        fontSize: 18,
        color: "black"
    },
    componenteDadosPerfil: {
        display: "flex",
        flex: 1,
        alignItems:"center",
        paddingTop:"12%",
    },
})
export default Estilo;