import React,{useState} from "react";
import {View,Image,Text,StyleSheet,TextInput,TouchableOpacity,Alert} from 'react-native';
import { useNavigation } from "@react-navigation/native";
import Estilo from "../Estilo";

const Home= (props) => {
    const navigation=useNavigation();   
    return (
        //é o que vai ser redenrizado
        <>
            <View style={Estilo.flexPai}>
                <View style={Estilo.menuPrincipal}>
                    <Text style={Estilo.textoHome}>
                        App - Cadastro de Alunos
                    </Text>
                </View>

                <TouchableOpacity
                    onPress={()=>navigation.navigate('Cadastro')}             
                    style={Estilo.menuPrincipal}
                >
                    <Text style={Estilo.textoHome}>
                        Cadastro
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={()=>navigation.navigate('Notas')}    
                    style={Estilo.menuPrincipal}          
                >
                    <Text style={Estilo.textoHome}>
                        Notas
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={()=>navigation.navigate('Perfil')}   
                    style={Estilo.menuPrincipal}           
                >
                    <Text style={Estilo.textoHome}>
                        Perfil do Aluno
                    </Text>
                </TouchableOpacity>
            </View>
        </>
    );
};
export default Home;