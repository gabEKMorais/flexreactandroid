import React, {useState} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
  Button,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import DatePicker from 'react-native-date-picker';
import Estilo from '../Estilo';
const Cadastro = ({navigation: {goBack}}) => {
  //printar route
  //console.log(route)

  //const {numeroSelecionado}=route.params

  //hooks
  const [codigo, setCodigo] = useState(null);
  const [nome, setNome] = useState(null);
  const [dataNascimento, setDataNascimento] = useState(new Date());
  const [open, setOpen] = useState(false); //abre e fecha o modal data..default fechado

  return (
    //é o que vai ser redenrizado

    <View style={Estilo.flexPai}>
      <View style={Estilo.menuPrincipal} />
      <View style={Estilo.componenteInputsCadastro}>
        <View>
          <Text style={Estilo.textoInputsCadastro}>Código</Text>
          <TextInput
            style={{width: 140, backgroundColor: '#ccc'}}
            onChangeText={setCodigo}
            value={codigo}
            selectTextOnFocus={true}
            keyboardType="numeric"
          />
        </View>
        <View>
          <Text style={Estilo.textoInputsCadastro}>Nome</Text>
          <TextInput
            style={{width: 300, backgroundColor: '#ccc'}}
            onChangeText={setNome}
            value={nome}
            selectTextOnFocus={true}
          />
        </View>
      </View>
      <View style={Estilo.menuPrincipal}>
        {/* https://github.com/henninghall/react-native-date-picker */}
        <Button title="Data" onPress={() => setOpen(true)} />
        <DatePicker
          modal
          open={open}
          mode={'date'}
          locale="pt-BR"
          style={{width: 250, backgroundColor: '#ccc'}}
          date={dataNascimento}
          onConfirm={date => {
            setOpen(false);
            setDataNascimento(date);
          }}
          onCancel={() => {
            setOpen(false);
          }}
        />
        <Text>`Data Nascimento: {dataNascimento.toDateString()}`</Text>      
      </View>
      <View style={Estilo.menuPrincipal}>
        <TouchableOpacity onPress={() => goBack()}>
          <Text style={Estilo.textoCadastrar}>Cancelar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          // https://reactnative.dev/docs/alert
          onPress={() =>
            Alert.alert(
              'Cadastro realizado',
              `Código:${codigo} Nome: ${nome} Data Nascimento: ${dataNascimento}}`,
              [
                {
                  text: 'Parabéns :)',
                  onPress: () => goBack(),
                },
              ],
            )
          }>
          <Text style={Estilo.textoCadastrar}>Cadastrar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Cadastro;
